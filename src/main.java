
import com.itextpdf.text.DocumentException;
import documentos.Banco;
import documentos.Cliente;
import documentos.Documentos;
import documentos.Grupo;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Cristhian Fabricio
 */
public class main {
   
    public static void main (String [ ] args) throws FileNotFoundException, DocumentException {
        
        LocalDate localDate= LocalDate.of(2022,03,17);
        Locale spanishLocale=new Locale("es", "ES");
        String dateInSpanish = localDate.format(DateTimeFormatter.ofPattern("EEEE, dd MMMM, yyyy",spanishLocale));
        Cliente cliente_1 = new Cliente("Cristhian","ocola","adsfadsf","asdfadf",73793438,950493974);
        Cliente cliente_2 = new Cliente("gian","ocola","adsfadsf","asdfadf",73793438,950493974);
        Grupo g = new Grupo("EL GRUPO","164","20000",dateInSpanish);
        g.add(cliente_1);
        g.add(cliente_2);
        Banco b =  new Banco("CAJA INCASUR ", "CAJA RURAL DE AHORRO Y CRÉDITO INCASUR S.A.", "Av. Vidaurrázaga N° 112-A, Parque Industrial","20455859728", " www.cajaincasur.com.pe.","AREQUIPA");
        Documentos doc = new Documentos(g,b);
    }
}
