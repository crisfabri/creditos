package documentos;

import java.time.LocalDate;
import java.util.ArrayList;

public class Grupo {
    
	String nombre;
	String Id;
	String saldo;
	ArrayList<Cliente> list;
        String fecha;
        String codigo_cred;
        String ciclo;
        
	public Grupo(String nombre, String id, String saldo,String fecha) {
		this.nombre = nombre;
		this.Id = id;
		this.saldo = saldo;
                this.fecha = fecha;
                codigo_cred = "0036548";
                ciclo = "";
                list = new ArrayList<Cliente>();

	}

    public String getCiclo() {
        return ciclo;
    }

    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }

    public String getCodigo_cred() {
        return codigo_cred;
    }

    public void setCodigo_cred(String codigo_cred) {
        this.codigo_cred = codigo_cred;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }


        
	public void add(Cliente user) {
		list.add(user);
		System.out.println("Agregado");
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getSaldo() {
		return saldo;
	}
	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}
	public ArrayList<Cliente> getList() {
		return list;
	}
	public void setList(ArrayList<Cliente> list) {
		this.list = list;
	}
	
}
