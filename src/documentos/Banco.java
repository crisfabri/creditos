
package documentos;

public class Banco {
	
	private String nombre;
	private String nombre_completo;
	private String correo;
	private String direccion;
	private String RUC;
	private String URL;
	private String Ciudad;
        private Integer interes_c;
        private Integer interes_m;
        
	public Banco(String nombre, String nombre_completo, String direccion, String rUC, String uRL,String ciudad) {
		super();
		this.nombre = nombre;
		this.nombre_completo = nombre_completo;
                this.Ciudad = ciudad;
		this.direccion = direccion;
		RUC = rUC;
		URL = uRL;
                interes_c = 8;
                interes_m = 5;
	}

    public Integer getInteres_c() {
        return interes_c;
    }

    public void setInteres_c(Integer interes_c) {
        this.interes_c = interes_c;
    }

    public Integer getInteres_m() {
        return interes_m;
    }

    public void setInteres_m(Integer interes_m) {
        this.interes_m = interes_m;
    }


    public String getCiudad() {
        return Ciudad;
    }

    public void setCiudad(String Ciudad) {
        this.Ciudad = Ciudad;
    }

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre_completo() {
		return nombre_completo;
	}

	public void setNombre_completo(String nombre_completo) {
		this.nombre_completo = nombre_completo;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getRUC() {
		return RUC;
	}

	public void setRUC(String rUC) {
		RUC = rUC;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}
	
	

}
