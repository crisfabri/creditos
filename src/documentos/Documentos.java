
package documentos;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Locale;


public class Documentos {
    
    Grupo grupo;
    Banco bank;
	
    public Documentos(Grupo grup, Banco bank) throws FileNotFoundException, DocumentException {
		grupo = grup;
		this.bank = bank;
                Contrato();
                Pagare();
                hoja_resumen();
    }

    public Documentos() throws FileNotFoundException, DocumentException {
        Contrato();
    }
  
    public void hoja_resumen() throws FileNotFoundException, DocumentException{
        
        LocalDate today = LocalDate.now();
        Locale spanishLocale=new Locale("es", "ES");
        String hoy = today.format(DateTimeFormatter.ofPattern("EEEE, dd MMMM, yyyy",spanishLocale));
        
        Font fuente = new Font(Font.FontFamily.TIMES_ROMAN, 10,
            Font.NORMAL, BaseColor.BLACK);
        Font fuente_2 = new Font(Font.FontFamily.TIMES_ROMAN, 10,
            Font.BOLD, BaseColor.BLACK);
        Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 14,
         Font.BOLD);
        Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
        PdfWriter.getInstance(doc, new FileOutputStream("hoja_resumen.pdf"));
        
        doc.open();
        Paragraph parrafo_1 = new Paragraph("HOJA RESUMEN GRUPAL \n\n",subFont);
        parrafo_1.setAlignment(Element.ALIGN_CENTER);
        doc.add(parrafo_1);
        
        Paragraph parrafo_2 = new Paragraph("El presente documento es parte integrante del Contrato de Crédito Grupal, suscrito con "+bank.getNombre_completo()+"., en adelante "+bank.getNombre()+" y en el que constan todas las condiciones de todos los créditos individuales otorgados a los clientes en forma global y conjunta, conforme al contrato mencionado.\n\n",fuente);
        parrafo_2.setIndentationLeft(30);
        parrafo_2.setIndentationRight(30);
        Paragraph parrafo_3 = new Paragraph("Agencia:\n",fuente_2);
        parrafo_3.setIndentationLeft(30);
        parrafo_3.setIndentationRight(30);
        Paragraph parrafo_4 = new Paragraph("DATOS DEL CRÉDITO \n\n",fuente_2);
        parrafo_4.setIndentationLeft(30);
        parrafo_4.setIndentationRight(30);
        
        
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(90);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
        table.getDefaultCell().setFixedHeight(80);
        PdfPCell celda_1 = new PdfPCell(new Paragraph("\n1- Nombre del Grupo\n",fuente));
        PdfPCell celda_2 = new PdfPCell(new Paragraph("\n "+grupo.getNombre()+"\n",fuente));
        PdfPCell celda_3 = new PdfPCell(new Paragraph("\n2. Código Crédito\n",fuente));
        PdfPCell celda_4 = new PdfPCell(new Paragraph("\n "+grupo.getCodigo_cred()+"\n",fuente));
        PdfPCell celda_5 = new PdfPCell(new Paragraph("\n3. Ciclo del Grupo\n",fuente));
        PdfPCell celda_6 = new PdfPCell(new Paragraph(" \n"+grupo.getCiclo()+"\n",fuente));
        PdfPCell celda_7 = new PdfPCell(new Paragraph("\n4. Moneda y Monto del Capital Prestado a EL GRUPO\n",fuente));
        PdfPCell celda_8 = new PdfPCell(new Paragraph(" \n"+grupo.getSaldo()+ " Nuevos Soles\n",fuente));
        PdfPCell celda_9 = new PdfPCell(new Paragraph("\n5. Fecha de Desembolso\n",fuente));
        PdfPCell celda_10 = new PdfPCell(new Paragraph("\n"+hoy+"\n",fuente));
        PdfPCell celda_11 = new PdfPCell(new Paragraph("\n6. Frecuencia de Pago\n",fuente));
        PdfPCell celda_12 = new PdfPCell(new Paragraph("\n \n",fuente));
        PdfPCell celda_13 = new PdfPCell(new Paragraph("\n7. Plazo de Prestamo Desembolsado\n",fuente));
        PdfPCell celda_14 = new PdfPCell(new Paragraph("\n \n",fuente));
        PdfPCell celda_15 = new PdfPCell(new Paragraph("\n8. Tasa de Interés Compensatorio Fija Efectiva Anual (TEA 360 días)\n",fuente));
        PdfPCell celda_16 = new PdfPCell(new Paragraph("\n \n",fuente));
        PdfPCell celda_17 = new PdfPCell(new Paragraph("\n9. Tasa de Costo Efectiva Anual \n",fuente));
        PdfPCell celda_18 = new PdfPCell(new Paragraph("\n \n",fuente));
        
        table.addCell(celda_1);
        table.addCell(celda_2);
        table.addCell(celda_3);
        table.addCell(celda_4);
        table.addCell(celda_5);
        table.addCell(celda_6);
        table.addCell(celda_7);
        table.addCell(celda_8);
        table.addCell(celda_9);
        table.addCell(celda_10);
        table.addCell(celda_11);
        table.addCell(celda_12);
        table.addCell(celda_13);
        table.addCell(celda_14);
        table.addCell(celda_15);
        table.addCell(celda_16);
        table.addCell(celda_17);
        table.addCell(celda_18);
        
        
        Paragraph parrafo_5 = new Paragraph("DATOS DE LOS PAGOS\n\n",fuente_2);
        parrafo_5.setIndentationLeft(30);
        parrafo_5.setIndentationRight(30);
        
        
        PdfPTable table_2 = new PdfPTable(2);
        table_2.setWidthPercentage(90);
        table_2.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table_2.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
        table_2.getDefaultCell().setFixedHeight(80);
        PdfPCell celda_1_2 = new PdfPCell(new Paragraph("\n10. Monto de la Cuota del Grupo\n",fuente));
        PdfPCell celda_2_2 = new PdfPCell(new Paragraph("\n \n",fuente));
        PdfPCell celda_3_2 = new PdfPCell(new Paragraph("\n11. Fecha del Primer Pago\n",fuente));
        PdfPCell celda_4_2 = new PdfPCell(new Paragraph("\n \n",fuente));
        PdfPCell celda_5_2 = new PdfPCell(new Paragraph("\n12. Fecha del Ultimo Pago\n",fuente));
        PdfPCell celda_6_2 = new PdfPCell(new Paragraph(" \n \n",fuente));
        
        table_2.addCell(celda_1_2);
        table_2.addCell(celda_2_2);
        table_2.addCell(celda_3_2);
        table_2.addCell(celda_4_2);
        table_2.addCell(celda_5_2);
        table_2.addCell(celda_6_2);
        
        
        Paragraph parrafo_6 = new Paragraph("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nCRONOGRAMA DE PAGOS GRUPAL\n\n",fuente_2);
        parrafo_6.setIndentationLeft(30);
        parrafo_6.setIndentationRight(30);
        
        PdfPTable table_3 = new PdfPTable(9);
        table_3.setWidthPercentage(90);
        table_3.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table_3.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
        table_3.getDefaultCell().setFixedHeight(80);
        PdfPCell celda_31 = new PdfPCell(new Paragraph("\nFECHA\n",fuente));
        PdfPCell celda_32 = new PdfPCell(new Paragraph("\nNRO.\n",fuente));
        PdfPCell celda_33 = new PdfPCell(new Paragraph("\nVALOR CUOTA\n",fuente));
        PdfPCell celda_34 = new PdfPCell(new Paragraph("\nCAPITAL\n",fuente));
        PdfPCell celda_35 = new PdfPCell(new Paragraph("\nINTERES COMP.\n",fuente));
        PdfPCell celda_36 = new PdfPCell(new Paragraph("\nSEGURO DESG. \n",fuente));
        PdfPCell celda_37= new PdfPCell(new Paragraph("\nITF\n",fuente));
        PdfPCell celda_38 = new PdfPCell(new Paragraph("\nSEG.INC. TODO RSGO \n",fuente));
        PdfPCell celda_39 = new PdfPCell(new Paragraph("\nSALDO CAPITAL \n",fuente));
        
        table_3.addCell(celda_31);
        table_3.addCell(celda_32);
        table_3.addCell(celda_33);
        table_3.addCell(celda_34);
        table_3.addCell(celda_35);
        table_3.addCell(celda_36);
        table_3.addCell(celda_37);
        table_3.addCell(celda_38);
        table_3.addCell(celda_39);
        
        for(int i=0;i<9*9;i++){
            PdfPCell celda_aux = new PdfPCell(new Paragraph("\n \n",fuente));
            table_3.addCell(celda_aux);
        }
        
        Paragraph parrafo_7 = new Paragraph("\nNotas: \n\n",fuente_2);
        parrafo_7.setIndentationLeft(30);
        parrafo_7.setIndentationRight(30);
        
        List list = new List();
        list.setIndentationLeft(30);
        list.setIndentationRight(50);
        ListItem item = new ListItem("1. Recuerde que debe realizar sus pagos completos en la fecha estipulada. ",fuente);
        ListItem item_2 = new ListItem("2. Recuerde que los pagos pueden ser realizados en cualquier agencia de "+bank.getNombre()+" o canales autorizados",fuente);
        ListItem item_3 = new ListItem("3. Se deja constancia de que los datos e información contenida en ésta Hoja Resumen Grupal y Cronograma de Pagos Grupal, constituyen un consolidado de todas las condiciones de los préstamos otorgados de manera individual a cada uno de las integrantes de LOS CLIENTES, y tiene como única finalidad facilitar el control de los pagos del crédito por parte de los representantes de LOS CLIENTES y de "+bank.getNombre()+". ",fuente);
        ListItem item_4 = new ListItem("4. Cualquier duda o comentario que tenga, favor de comunicarse en la agencia que le corresponda. ",fuente);
        
       list.add(item);
       list.add(item_2);
       list.add(item_3);
       list.add(item_4);
        
       
       
        Paragraph parrafo_8 = new Paragraph("\nREPRESENTANTE DEL GRUPO           SELLO Y FIRMA - REPRESENTANTE  \n\n",fuente_2);
        parrafo_8.setAlignment(Element.ALIGN_CENTER);
        
        
        Paragraph parrafo_9 = new Paragraph("\nNOMBRE:\n",fuente_2);
        parrafo_9.add("DNI:\n\n");
        parrafo_9.add("FIRMA: \n");
        parrafo_9.setIndentationLeft(30);
        parrafo_9.setIndentationRight(30);
        
        doc.add(parrafo_2);
        doc.add(parrafo_3);
        doc.add(parrafo_4);
        doc.add(table);
        doc.add(parrafo_5);
        doc.add(table_2);
        doc.add(parrafo_6);
        doc.add(table_3);
        doc.add(parrafo_7);
        doc.add(list);
        doc.add(parrafo_8);
        doc.add(parrafo_9);
        
        doc.close();
        
    }
    
    public void Pagare() throws FileNotFoundException, DocumentException{
        
        LocalDate today = LocalDate.now();
        Locale spanishLocale=new Locale("es", "ES");
        String hoy = today.format(DateTimeFormatter.ofPattern("EEEE, dd MMMM, yyyy",spanishLocale));

        
        Font fuente = new Font(Font.FontFamily.TIMES_ROMAN, 10,
            Font.NORMAL, BaseColor.BLACK);
        Font fuente_2 = new Font(Font.FontFamily.TIMES_ROMAN, 10,
            Font.BOLD, BaseColor.BLACK);
        Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 14,
         Font.BOLD);
        Document doc = new Document();
        PdfWriter.getInstance(doc, new FileOutputStream("pagare.pdf"));

        doc.open();
        Paragraph parrafo_1 = new Paragraph("PAGARE\n\n",subFont);
        parrafo_1.setAlignment(Element.ALIGN_CENTER);
        
        Paragraph parrafo_2 = new Paragraph("    NUMERO:    "+grupo.getId()+"                                                                                               MONTO: "+grupo.getSaldo()+"\n\n",fuente_2);
        
        
        Paragraph parrafo_3 = new Paragraph("                                                                                                                      VENCIMIENTO: "+grupo.getFecha()+"\n\n",fuente_2);
        
        
        Paragraph parrafo_4 = new Paragraph("Me/nos obligo/obligamos a pagar solidaria e incondicionalmente a la orden de "+bank.getNombre()+" la suma de "+grupo.getSaldo()+", monto recibido en dinero en efectivo a mi/nuestra entera satisfacción, obligación que cumpliré/cumpliremos al vencimiento de este Título Valor. Asimismo me/nos obligo/obligamos al cumplimiento de  las siguientes cláusulas: \n\n",fuente);
        
        
        List list = new List();
        list.setIndentationLeft(30);
        list.setIndentationRight(50);
        list.setListSymbol("-");
        ListItem item = new ListItem("El/los emitente/s acepto/aceptamos las prorrogas y renovaciones totales o parciales que se efectúen al vencimiento de este título valor, sin necesidad de nuestra intervención.",fuente);
        ListItem item_2 = new ListItem("Por la suma de dinero recibida me/nos obligamos a pagar un interés compensatorio del  "+bank.getInteres_c()+"% anual; en caso de incumplimiento de la prestación en la fecha de vencimiento pagaré/pagaremos un interés moratorio del "+bank.getInteres_m()+"% anual. La mora del/los emitente/s será automática, sin previa intimación del tenedor o acreedor. ",fuente);
        ListItem item_3 = new ListItem("El presente pagaré no estará sujeto a protesto por falta de pago, procediendo su ejecución por el sólo mérito de haber vencido su plazo. ",fuente);
        ListItem item_4 = new ListItem("El/los emitente/s autoriza/autorizamos la destrucción del presente titulo valor a su cancelación de acuerdo con el Artículo 17.2 de la Ley 27287. ",fuente);
        ListItem item_5 = new ListItem("Este Título Valor será cancelado en las oficinas de "+bank.getNombre()+" o en el lugar donde se presente para su cobro.",fuente);
        ListItem item_6 = new ListItem("El/los emitente/s renuncio/renunciamos al fuero de mi/nuestro domicilio, siendo ejecutable este pagaré ante los juzgados del cercado de la ciudad de Arequipa o donde se presente para su cobro. Para todos los efectos de este pagaré señalo/señalamos nuestro/s domicilio/s en la dirección  abajo indicada. \n\n",fuente);
        
        list.add(item);
        list.add(item_2);
        list.add(item_3);
        list.add(item_4);
        list.add(item_5);
        list.add(item_6);
        
         Paragraph parrafo_5 = new Paragraph("                                                                                                                          "+bank.getCiudad()+", "+hoy+"\n\n",fuente);
        
        
        
        doc.add(parrafo_1);
        doc.add(parrafo_2);
        doc.add(parrafo_3);
        doc.add(parrafo_4);
        doc.add(list);
        doc.add(parrafo_5);
        
        System.out.println(grupo.getList().size());
        for(int i=0; i<grupo.getList().size();i++){
            
            Paragraph parrafo_6 = new Paragraph();
            parrafo_6.setFont(fuente);
            parrafo_6.add("\n\nFirma: \n");
            parrafo_6.add("________________________________________________________________________________________\n");
            parrafo_6.add("(Del cliente)\n\n");
            parrafo_6.add("Nombre: "+grupo.getList().get(i).getNombre()+" "+grupo.getList().get(i).getApellido()+"                                                                     DNI: "+grupo.getList().get(i).getDni()+"\n\n");
            parrafo_6.add("Domicilio: "+grupo.getList().get(i).getDireccion()+"\n");
            parrafo_6.setIndentationLeft(40);
            parrafo_6.setIndentationRight(40);
            
            doc.add(parrafo_6);
        }
        
        doc.close();
    }
    
    public  void Contrato() throws FileNotFoundException, DocumentException{
        
        Font fuente = new Font(Font.FontFamily.TIMES_ROMAN, 10,
            Font.NORMAL, BaseColor.BLACK);
        Font fuente_2 = new Font(Font.FontFamily.TIMES_ROMAN, 10,
            Font.BOLD, BaseColor.BLACK);
        Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 14,
         Font.BOLD);
        Document doc = new Document();
        PdfWriter.getInstance(doc, new FileOutputStream("Contrato.pdf"));
        doc.open();
        
        Paragraph parrafo_1 = new Paragraph("CONTRATO DE CRÉDITO GRUPAL\n\n",subFont);
        parrafo_1.setAlignment(Element.ALIGN_CENTER);
        
        Paragraph parrafo_2 = new Paragraph("Conste por el presente documento, el contrato de prestamo denominado Credito Grupal "+ bank.getNombre() +" que celebran de una parte, "+bank.getNombre_completo()+" con RUC "+bank.getRUC()+" con domicilio en "+bank.getDireccion()+" "+bank.getCiudad()+", debidamente representado por el ejecutivo que firma la presente, en merito a los poderes inscritos en la partida N° 111179010 del registro de personas Jurídicas de la Oficina registral XII con sede en "+bank.getCiudad()+", a quien en adelante se denominar "+bank.getNombre()+" de la otra, las personas cuos datos  firmas aparecen en la parte final de este contrato, a quienes en adelante se denominar "+grupo.getNombre()+", en los terminos  condiciones siguientes: \n\n",fuente);
        
        Paragraph parrafo_3 = new Paragraph("PRIMERA.- ",fuente_2);
        parrafo_3.setFont(fuente);
        parrafo_3.add(" A solicitud de LOS CLIENTES, "+bank.getNombre()+" otorga un crédito a favor de cada uno de LOS CLIENTES por la cantidad que se indica en su respectiva HOJA RESUMEN y en el formato denominado como SOLICITUD DE CRÉDITO. Los créditos otorgados a LOS CLIENTES se rigen por el presente contrato y forman parte de él como anexos que se entregan a LOS CLIENTES, la hoja resumen grupal, cronograma de pagos grupal,  la hoja resumen individual y cronograma de pagos individual, en el que se consigna la cuota individual  que será equivalente al monto prorrateado  del pago grupal mensual que le corresponde a cada CLIENTE. Los intereses, comisiones y gastos del crédito grupal serán cobrados proporcionalmente a cada CLIENTE en su cronograma individual, de manera tal que el cronograma de pagos grupal, sólo reflejará la suma de las cuotas que LOS CLIENTES deben pagar cada mes conforme a su fecha de vencimiento.\n\n");
        
        Paragraph parrafo_4 = new Paragraph("SEGUNDA.- ",fuente_2);
        parrafo_4.setFont(fuente);
        parrafo_4.add("LOS CLIENTES a la firma de este Contrato, se obligan a destinar el importe de sus créditos para aquella actividad económica establecida en el formato SOLICITUD DE CRÉDITO, la cual debe ser una actividad lícita. Asimismo LOS CLIENTES declaran y reconocen expresamente que han sido correcta y oportunamente instruidos sobre las condiciones del crédito, señaladas en la HOJA RESUMEN y que adicionalmente a los intereses compensatorios pactados, "+bank.getNombre()+" cobrará intereses moratorios en caso de incumplimiento en el pago oportuno de las obligaciones a cargo de LOS CLIENTES a la tasa pactada, para lo cual no será necesario requerimiento alguno de pago para constituir en mora a LOS CLIENTES pues es entendido que ésta se producirá de modo automático a partir del vencimiento de las obligaciones asumidas en este contrato y que se indican en sus cronogramas de pagos.\n\n");
        
        Paragraph parrafo_5 = new Paragraph("TERCERA.- ",fuente_2);
        parrafo_5.setFont(fuente);
        parrafo_5.add(" LOS CLIENTES declaran haberse agrupado con el propósito de obtener un crédito conjunto, otorgándose en forma recíproca y entre sí, aval solidario a fin de asumir un riesgo compartido y superar las dificultades formales para acceder a un préstamo individual. Por consiguiente, acordaron en agruparse con la denominación que aparece en la parte final del presente contrato, y al que en lo sucesivo se denominara como "+grupo.getNombre()+", asumiendo solidariamente el cumplimiento total y oportuno de las obligaciones que se contraen en este contrato. "+grupo.getNombre()+", por la mayoría de votos ha designado a dos de sus integrantes como representantes de "+grupo.getNombre()+", una con la calidad de presidente(a)  y la otra de tesorero (a), cuyos nombres y responsabilidades obran en el Acta de Compromiso que forma parte del presente contrato.\n\n");
        
        Paragraph parrafo_6 = new Paragraph("CUARTA.- ",fuente_2);
        parrafo_6.setFont(fuente);
        parrafo_6.add("El conjunto de los créditos otorgados a cada uno de LOS CLIENTES, asciende a la cantidad señalada en la HOJA RESUMEN de cada cliente. LOS CLIENTES están de acuerdo con el monto señalado y se obligan a cumplir en la forma, términos y condiciones convenidas en este contrato, obligándose a pagar a "+bank.getNombre()+" el importe otorgado más los intereses,  comisiones y gastos que se estipulen o generen hasta el día de la liquidación total del préstamo.\n\n");
        
        Paragraph parrafo_7 = new Paragraph("QUINTA.- ",fuente_2);
        parrafo_7.setFont(fuente);
        parrafo_7.add("El crédito será pagado por LOS CLIENTES mediante cuotas periódicas, fijadas por el importe total de la cuota del crédito grupal, que equivale a la suma de las cuotas establecidas en el cronograma de pagos individual, en la misma moneda desembolsada y en la fecha establecida en los cronogramas de pagos. Cada crédito será pagado por LOS CLIENTES en forma conjunta mediante cuotas periódicas, con la frecuencia, fecha, moneda y monto detallados en el CRONOGRAMA DE PAGOS que forma parte de la HOJA RESUMEN GRUPAL.\n\n");

        
        Paragraph parrafo_8 = new Paragraph("SEXTA.- ",fuente_2);
        parrafo_8.setFont(fuente);
        parrafo_8.add("LOS CLIENTES dispondrán de su crédito en una sola armada en efectivo, a favor de cada uno de los integrantes de "+grupo.getNombre()+", por las cantidades pactadas y aprobadas. Para ello, cada CLIENTE de "+grupo.getNombre()+" deberá abrir a su nombre una cuenta de ahorros en "+bank.getNombre()+" donde deberán depositar un ahorro de no menos del 10% del monto solicitado y/o aprobado como crédito, el que se mantendrá bloqueado durante la vigencia del crédito. Cada CLIENTE deberá suscribir el contrato y Cartilla de Información respectiva para la apertura de la cuenta de ahorros.\n\n");
        
        Paragraph parrafo_9= new Paragraph("SÉTIMA.- ",fuente_2);
        parrafo_9.setFont(fuente);
        parrafo_9.add("Todos los pagos y/o depósitos deberán realizarse en efectivo, dichos pagos serán depositados en cualquiera de las agencias de "+bank.getNombre()+", cuya ubicación se consigna en la página web "+bank.getURL()+". Estos pagos incluyen, la amortización del capital, los intereses, comisiones y gastos. LOS CLIENTES, tienen derecho a efectuar pagos anticipados de las cuotas o de los saldos en forma total o parcial, con la consiguiente reducción de los intereses, las comisiones y gastos derivados de las cláusulas de este contrato al día de pago, sin que le sean aplicables comisiones, gastos o penalidades de algún tipo o cobro de naturaleza o efecto similar. En los casos que se efectúen pagos anticipados parciales (que es cuando se realiza un pago mayor a la cuota del período pero menor al total de la obligación), LOS CLIENTES señalarán si debe procederse a la reducción del monto o del número de cuotas. En tanto se trata de una modificación a las condiciones contractuales, estas operaciones requerirán expreso consentimiento de LOS CLIENTES, a quienes se hará entrega de un nuevo CRONOGRAMA DE PAGOS INDIVIDUAL, al momento de efectuar el pago anticipado, el cual consignará el saldo remanente del crédito individual otorgado y, un nuevo CRONOGRAMA DE PAGOS GRUPAL que indique el saldo remanente del préstamo grupal.\n\n");
        
        Paragraph parrafo_10 = new Paragraph("OCTAVA.- ",fuente_2);
        parrafo_10.setFont(fuente);
        parrafo_10.add(""+bank.getNombre()+" aplicará  las cantidades que reciba en pago conforme al siguiente orden de prelación: impuestos, intereses moratorios, intereses compensatorios  acordados y el capital.\n\n");
        
        Paragraph parrafo_11 = new Paragraph("NOVENA.- ",fuente_2);
        parrafo_11.setFont(fuente);
        parrafo_11.add("En concordancia con lo estipulado en art. 10 de la Ley de Títulos Valores, la Circular SBS G-0090-2001 o cualquier norma que la modifique o sustituya, el préstamo otorgado está representado por un pagaré emitido y aceptado en forma incompleta por LOS CLIENTES, mediante la suscripción de todos los integrandes de "+grupo.getNombre()+". Las partes convienen que, en cualquier caso de incumplimiento de las obligaciones asumidas por LOS CLIENTES, "+bank.getNombre()+" podrá completar y ejecutar el pagaré con el saldo adeudado incluyendo los intereses compensatorios y moratorios, comisiones y gastos generados. El pagaré será llenado en la fecha en que este venza o en la que "+bank.getNombre()+" opte  dar por vencidos todos los plazos de la obligación\n. EL CLIENTE deja constancia que, al momento de suscripción del presente contrato, recibe copia del pagaré antes referido tomando conocimiento de los requisitos necesarios para completarlo en caso sea necesaria su ejecución. Asimismo, declara haber sido informado sobre los mecanismos que la ley permite para la emisión y aceptación de títulos valores incompletos. EL CLIENTE renuncia en forma expresa a la inclusión de la cláusula que limite o restrinja la transmisibilidad de este título valor, facultando asimismo a su tenedor a renovarlo y/o prorrogarlo siempre que cuente con su autorización o intervención.\n\n");
        
        Paragraph parrafo_12 = new Paragraph("DÉCIMA.- ",fuente_2);
        parrafo_12.setFont(fuente);
        parrafo_12.add("EL CLIENTE faculta a "+bank.getNombre()+" para que, en caso de incumplimiento de cualquiera de las obligaciones vencidas y exigibles establecidas en el presente contrato, pueda retener y/o aplicar a la amortización o cancelación de sus adeudos, sean estos por capital, intereses, comisiones, gastos, tributos o seguros aplicable al préstamo, todos los saldos acreedores que EL CLIENTE tenga o pudiera tener en cualquier cuenta que mantenga en "+bank.getNombre()+", así como cualquier otro monto que, por cualquier causa, "+bank.getNombre()+" tenga o pudiera tener en su poder y sea destinado a ser entregado o acreditado a EL CLIENTE, salvo las limitaciones legales que pudieran existir en esta materia\n. Si "+bank.getNombre()+" ejecuta la compensación, comunicará por escrito a EL CLIENTE que la realizó, indicando el motivo de su ejecución  y los montos abonados a favor de "+bank.getNombre()+" en un plazo no mayor de 30 días.\n\n");
        
        Paragraph parrafo_13 = new Paragraph("DÉCIMA PRIMERA.- ",fuente_2);
        parrafo_13.setFont(fuente);
        parrafo_13.add("EL CLIENTE y EL (LOS) AVAL (ES) otorga (n) su expreso consentimiento a "+bank.getNombre()+" para que a su criterio y conforme al Código Civil ceda sus derechos y/o su posición contractual en este crédito y las garantías que lo respaldan a favor de terceros. Esta cesión se hará efectiva con la comunicación escrita que se efectúe a EL CLIENTE.\n\n\n\n\n\n\n\n");
        
        Paragraph parrafo_14 = new Paragraph("DÉCIMA SEGUNDA.- ",fuente_2);
        parrafo_14.setFont(fuente);
        parrafo_14.add(""+bank.getNombre()+" podrá dar por resuelto el presente contrato, cuando cualquiera de LOS CLIENTES incurra en cualquiera de las causales, previo requerimiento escrito cursado ese CLIENTE con 3 días hábiles de  anticipación, cuando se presente alguno de los siguientes casos:\n\n");
        
        List list = new List();
        list.setIndentationLeft(50);
        list.setIndentationRight(50);
        ListItem item = new ListItem("a) Si LOS CLIENTES incumplen con el pago de una o más cuotas establecidas en el cronograma de pagos grupal.",fuente);
        ListItem item_2 = new ListItem("b) Si de ser el caso LOS CLIENTES suspenden sus actividades comerciales.",fuente);
        ListItem item_3 = new ListItem("c) Si LOS CLIENTES se someten voluntariamente o son sometidos por sus acreedores a cualquier procedimiento concursal. ",fuente);
        ListItem item_4 = new ListItem("d) Si el crédito otorgado fuera destinado a la consecución de cualquier acto ilícito. ",fuente);
        ListItem item_5 = new ListItem("e) Si LOS CLIENTES no informan a "+bank.getNombre()+" de cualquier situación que razonablemente pudiera afectar su situación patrimonial, la recuperación de los créditos y/o la disponibilidad de sus bienes. ",fuente);
        item.setAlignment(Element.ALIGN_JUSTIFIED);
        
        list.add(item);
        list.add(item_2);
        list.add(item_3);
        list.add(item_4);
        list.add(item_5);
        
        
        Paragraph parrafo_15 = new Paragraph("\nDÉCIMA TERCERA.- ",fuente_2);
        parrafo_15.setFont(fuente);
        parrafo_15.add("LOS CLIENTES autorizan a "+bank.getNombre()+" a:\n\n");
        
        List list_2 = new List();
        list_2.setIndentationLeft(50);
        list_2.setIndentationRight(50);
        ListItem item2 = new ListItem("a) Utilizar y/o proporcionar la información de los documentos relacionados con la operación del crédito, a sus subsidiarias, filiales o cualquier empresa controlada directa o indirectamente por "+bank.getNombre()+", para efecto de actividades promocionales o para ofrecer operaciones y servicios prestados por dichas empresas. ",fuente);
        ListItem item_22 = new ListItem("b) Proporcionar y recabar información sobre operaciones crediticias con "+bank.getNombre()+" a las empresas de información crediticia y/o centrales de riesgo o quienes hagan de ellas. ",fuente);
        ListItem item_23 = new ListItem("c) Permitir que personal de "+bank.getNombre()+" o cualquier otra institución o persona que esta designe, pueda realizar visitas a sus domicilios o lugar donde desarrollan sus labores, a efecto de verificar el desarrollo de sus negocios. ",fuente);
        ListItem item_24 = new ListItem("d) Llamar o enviar mensajes a sus domicilios o teléfonos celulares, o de usar cualquier forma y medio de comunicación, visitarlos contactarlos y/o ubicarlos en cualquier lugar, para informarles de los servicios o aspectos de los productos que "+bank.getNombre()+" ofrece, así como para efectos de cobranza permitidos por la ley. ",fuente);
        item2.setAlignment(Element.ALIGN_JUSTIFIED);
        list_2.add(item2);
        list_2.add(item_22);
        list_2.add(item_23);
        list_2.add(item_24);
        
        Paragraph parrafo_16 = new Paragraph("\nDÉCIMA CUARTA.- .- ",fuente_2);
        parrafo_16.setFont(fuente);
        parrafo_16.add(""+bank.getNombre()+" se reserva el derecho de modificar los alcances del presente contrato. Las Partes acuerdan que "+bank.getNombre()+", cuando las condiciones del mercado y/o sus políticas de crédito así lo ameriten, podrá modificar unilateralmente las  comisiones, gastos y demás estipulaciones de este contrato. Para el caso de las tasas de interés, estas serán modificadas por novación o negociación efectiva entre las partes y cuando la Superintendencia de Banca, Seguros y AFP lo autorice, previo informe favorable del Banco Central de reserva del Perú. "+bank.getNombre()+"  deberá comunicar a través de medios de comunicación directa, tales como comunicaciones escritas a sus domicilios, correos electrónicos, estados de cuenta y comunicaciones telefónicas a EL CLIENTE, con 45 días de anticipación a su entrada en vigencia, las modificaciones contractuales referidas a:\n\n");
        
        List list_3 = new List();
        list_3.setListSymbol("-");
        list_3.setIndentationLeft(50);
        list_3.setIndentationRight(50);
        ListItem item3 = new ListItem("Penalidades, comisiones, gastos y cronograma de pagos,  cuando dichas modificaciones generen perjuicio a los usuarios",fuente);
        ListItem item_32 = new ListItem("La variación de la tasa de interés.",fuente);
        ListItem item_33 = new ListItem("La resolución del contrato por causal distinta al incumplimiento. ",fuente);
        ListItem item_34 = new ListItem("La limitación o exoneración de responsabilidad por parte de las empresas. ",fuente);
        ListItem item_35 = new ListItem("Incorporación de servicios que no se encuentren directamente relacionados al producto o servicio contratado. ",fuente);
        item3.setAlignment(Element.ALIGN_JUSTIFIED);
        list_3.add(item3);
        list_3.add(item_32);
        list_3.add(item_33);
        list_3.add(item_34);
        list_3.add(item_35);
        
        Paragraph parrafo_17 = new Paragraph("\nAdicionalmente, podrá publicar dichos cambios mediante su página Web y en las Agencias de "+bank.getNombre()+" con una anticipación no menor de cuarenta y cinco (45) días calendario a su entrada en vigencia. Si estas modificaciones perjudican a EL CLIENTE y varían lo informado en el cronograma de pagos, este deberá ser recalculado y remitido al cliente con la TCEA remanente y con 45 días de anticipación a su entrada en vigencia.\n",fuente);

        
        Paragraph parrafo_18 = new Paragraph("Si EL CLIENTE no aceptase las modificaciones señaladas en los párrafos precedentes, se obliga a manifestar a "+bank.getNombre()+" su disconformidad por escrito en un plazo máximo de 45 días calendarios contados a partir de la recepción de la comunicación de modificación, en cuyo caso, una vez vencido dicho plazo, se resolverá el presente contrato siempre y cuando EL CLIENTE  cancele dentro de este término, el íntegro del saldo que estuviere adeudando a "+bank.getNombre()+" con relación a el(los) crédito(s) materia del presente contrato, de lo contrario, este mantendrá su vigencia y se darán por aceptadas las modificaciones contractuales por EL CLIENTE.\nLas modificaciones contractuales de aspectos distintos a los indicados anteriormente en esta cláusula o las que sean favorables para EL CLIENTE, deberán ser comunicadas utilizando medios escritos y en la página  "+bank.getURL()+", o medio que permitan a EL CLIENTE tomar conocimiento adecuado y oportuno de las mismas, acordando para este efecto que "+bank.getNombre()+" podrá utilizar medios directos en la medida que esto sea posible, su página web o publicidad en sus oficinas. Estas modificaciones, entrarán en vigencia a los cuarenta y cinco (45) días de haber sido comunicadas a EL CLIENTE a través de los medios antes indicados.\nLa negativa  por parte de EL CLIENTE  a la incorporación de servicios que no se encuentren directamente relacionados al producto o servicio contratado, no implica la resolución del presente contrato.\nCuando "+bank.getNombre()+" otorgue a EL CLIENTE condiciones, opciones o derechos que constituyan facilidades adicionales a las existentes y que no impliquen la pérdida ni la sustitución de condiciones previamente establecidas, no serán considerados como modificaciones contractuales, y por lo tanto, no serán comunicados previamente a EL CLIENTE, pudiendo éstas aplicarse de manera inmediata y serán comunicadas posteriormente dentro de los 45 días siguientes a su aplicación, a través de su página web.\n\n",fuente);
        
        
        Paragraph parrafo_19_ = new Paragraph("DÉCIMA QUINTA.- ",fuente_2);
        parrafo_19_.setFont(fuente);
        parrafo_19_.add("EL CLIENTE deberá contratar un seguro de desgravamen,  a través de "+bank.getNombre()+" o por cuenta de terceros, en la compañía de seguros de su elección; debiendo EL CLIENTE cumplir con las condiciones mínimas señaladas en la página web ("+bank.getURL()+"). En caso "+bank.getNombre()+" contrate por cuenta de EL CLIENTE, el seguro de desgravamen, este mantendrá su vigencia por todo el plazo del crédito.\n"
                + "En el caso del seguro de desgravamen, "+bank.getNombre()+" será la única beneficiaria cuando el seguro sea por saldo deudor; y cuando el seguro sea por el importe de la deuda, "+bank.getNombre()+" será beneficiaria, sólo por el importe adeudado y el saldo será entregado a EL CLIENTE. Este seguro cubrirá el saldo del capital adeudado a la fecha de fallecimiento del titular nombrado y/o su cónyuge, de acuerdo a la modalidad de seguro de desgravamen de crédito que contrate EL CLIENTE, hasta por la cobertura máxima convenida con la empresa aseguradora. Esta autorización se mantendrá subsistente durante la vigencia del crédito. En caso EL CLIENTE contrate directamente su seguro,  se obliga a endosar la póliza correspondiente a favor de "+bank.getNombre()+".\n"
                + "EL CLIENTE declara tener pleno conocimiento y aceptar las condiciones establecidas para la implementación y ejecución de los seguros antes referidos, así como los riesgos cubiertos, el monto de la prima o la forma de determinarla, las exclusiones del seguro y el plazo para efectuar reclamos.\n"
                + "Cuando "+bank.getNombre()+" contrate directamente el seguro, a decisión de "+bank.getNombre()+", el importe de la prima será pagado en efectivo por EL CLIENTE,  para lo cual otorga por el presente la autorización correspondiente;  o incluido en las cuotas de pago del préstamo.\n"
                + "Una vez que reciba la póliza o certificado del seguro de la compañía de seguros, "+bank.getNombre()+" la pondrá a disposición de EL CLIENTE en sus oficinas, a efecto de que este la recoja cuando lo considere conveniente. Todos los alcances de la presente cláusula, serán aplicables también en caso de renovaciones o ampliaciones de la póliza o certificado del (los) seguro (s) y las condiciones mínimas del seguro indicado, se encuentran consignados en la página web de "+bank.getNombre()+" "+bank.getURL()+"\n\n\n\n\n\n\n\n\n\n");

        Paragraph parrafo_19 = new Paragraph("DÉCIMA SEXTA.- ",fuente_2);
        parrafo_19.setFont(fuente);
        parrafo_19.add(" En aplicación del artículo 85 del código de protección y defensa del consumidor, aprobado por ley número 29571, "+bank.getNombre()+" podrá modificar el presente contrato, en aspectos distintos a las tasas de interés, comisiones o gastos, e incluso resolverlos sin aviso previo, como consecuencia de la aplicación de las normas prudenciales emitidas por la Superintendencia de Banca,  Seguros y AFPs (Circular  SBS N° 253-2011), tales como:\n\n");
        
        
        List list_4 = new List();
        list_4.setIndentationLeft(50);
        list_4.setIndentationRight(50);
        ListItem item4 = new ListItem("a) Falta de transparencia de LOS CLIENTES en la información señalada o presentada para la evaluación realizada antes de la contratación; durante la relación contractual si se desprende que dicha información es inexacta, incompleta, falsa o inconsistente con la información previamente declarada o entregada por LOS CLIENTES y esta, repercuta negativamente en el riesgo de reputación o legal que enfrenta "+bank.getNombre()+". ",fuente);
        ListItem item_42 = new ListItem("b) Por consideraciones del perfil de LOS CLIENTES vinculadas a la prevención del lavado de activos o del financiamiento del terrorismo. ",fuente);
        ListItem item_43 = new ListItem("c) Sobreendeudamiento de LOS CLIENTES. ",fuente);
        ListItem item_44 = new ListItem("d) En estos casos "+bank.getNombre()+" remitirá una comunicación escrita a LOS CLIENTES, dentro de los 7 días posteriores a dicha modificación o resolución, en la que señalará que dicha resolución o modificación se realiza sobre la base del dispositivo legal indicado. En caso de modificarse el contrato, LOS CLIENTES podrán resolver el contrato mediante comunicación escrita, procediendo a pagar todo el saldo u obligación que mantuviera pendiente. ",fuente);
        item4.setAlignment(Element.ALIGN_JUSTIFIED);
        list_4.add(item4);
        list_4.add(item_42);
        list_4.add(item_43);
        list_4.add(item_44);
        
        Paragraph parrafo_20= new Paragraph("\nDÉCIMA SÉTIMA.- ",fuente_2);
        parrafo_20.setFont(fuente);
        parrafo_20.add("Las partes contratantes convienen que en tanto no se comunique por escrito el cambio de domicilio de cualquiera de estas, cualquier diligencia, actuación, notificación, emplazamiento, requerimiento o comunicación, surtirá efectos legales en los domicilios señalados en el presente contrato.\n\n");
        
        Paragraph parrafo_21= new Paragraph("DÉCIMA OCTAVA.- ",fuente_2);
        parrafo_21.setFont(fuente);
        parrafo_21.add("Para la interpretación y cumplimiento de este Contrato, las partes que intervienen en el presente instrumento se someten a la jurisdicción y competencia de los tribunales que correspondan al del lugar en que se suscriben este contrato, o a los tribunales judiciales de la ciudad de Arequipa, a elección de "+bank.getNombre()+", renunciando a cualquier otro fuero que por razón de sus domicilio presente o futuro, les pudiera corresponder.\n\n");
        
        Paragraph parrafo_22= new Paragraph("DÉCIMA NOVENA.- ",fuente_2);
        parrafo_22.setFont(fuente);
        parrafo_22.add("En todo lo no previsto por el presente contrato, el crédito grupal que LOS CLIENTES mantenga en "+bank.getNombre()+" estará sujeta supletoriamente a las disposiciones de la Ley 26702, Ley General del Sistema Financiero y del Sistema de Seguros y Orgánica de la Superintendencia de Banca y Seguros, las normas del Código Civil, aquellas normas que las modifiquen, así como a las demás normas legales que le resulten aplicables.\n\n");
        
        Paragraph parrafo_23= new Paragraph("VIGÉSIMA.- ",fuente_2);
        parrafo_23.setFont(fuente);
        parrafo_23.add("LOS CLIENTES declaran que han sido informados de todas las condiciones y características de este producto crediticio, y que están conformes con ellas, que cada uno de los miembros de"+grupo.getNombre()+" ha recibido su respectiva Hoja Resumen y Cronograma de Pagos, y asimismo acuerdan que la copia del presente Contrato de Crédito, le sea entregada a la persona elegida como Presidente de Grupo.\n\n");
        
        Paragraph parrafo_24= new Paragraph("ENTERADOS DEL CONTENIDO y alcance jurídico de las obligaciones que contraten las partes contratantes con la celebración de este contrato de cláusulas generales, LOS CLIENTES suscriben, manifestando que tienen conocimiento y comprenden plenamente la obligación que adquieren, aceptando el monto del crédito que se les otorga, así como los cargos y gastos que se generen, o en su caso, se llegaran a generar por motivo de su suscripción, entendiendo, por lo que firman de conformidad en el lugar y fecha que se menciona en el presente contrato. ",fuente);
        
        
        doc.add(parrafo_1);
        doc.add(parrafo_2);
        doc.add(parrafo_3);
        doc.add(parrafo_4);
        doc.add(parrafo_5);
        doc.add(parrafo_6);
        doc.add(parrafo_7);
        doc.add(parrafo_8);
        doc.add(parrafo_9);
        doc.add(parrafo_10);
        doc.add(parrafo_11);
        doc.add(parrafo_12);
        doc.add(parrafo_13);
        doc.add(parrafo_14);
        doc.add(list);
        doc.add(parrafo_15);
        doc.add(list_2);
        doc.add(parrafo_16);
        doc.add(list_3);
        doc.add(parrafo_17);
        doc.add(parrafo_18);
        doc.add(parrafo_19_);
        doc.add(parrafo_19);
        doc.add(list_4);
        doc.add(parrafo_20);
        doc.add(parrafo_21);
        doc.add(parrafo_22);
        doc.add(parrafo_23);
        doc.add(parrafo_24);
 
        doc.close();
    }
   
    
    
}
